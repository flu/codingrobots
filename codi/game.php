<?php

$var = $_POST['var'];
$servername = "";
$username = "";
$password = "";

$dbname = "";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//echo $_GET['rid'];
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "UPDATE list_robots SET activated = '1' WHERE list_robots.robotID = ".$var;

$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
  <head>
    <script src="https://aframe.io/releases/0.8.2/aframe.min.js"></script>
    <script src="//cdn.rawgit.com/donmccurdy/aframe-extras/v4.2.0/dist/aframe-extras.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/deepstream.io-client-js/2.1.1/deepstream.js"></script>
    <script src="https://rawgit.com/donmccurdy/aframe-extras/v2.1.1/dist/aframe-extras.loaders.min.js"></script>
    <script src="https://unpkg.com/aframe-text-geometry-component@0.5.1/dist/aframe-text-geometry-component.min.js"></script>
    <script src="https://rawgit.com/feiss/aframe-environment-component/master/dist/aframe-environment-component.min.js"></script>
    <script type="text/javascript" src="main.js"></script>
      <link rel="stylesheet" href="style.css">
    <script>
      var gamePieces = 0;
    // Component to change to a sequential color on click.
      AFRAME.registerComponent('cursor-listener', {
        init: function () {
          var lastIndex = -1;
          var COLORS = ['red', 'gray', 'blue'];
          this.el.addEventListener('click', function (evt) {
            var  dbParam, xmlhttp;
            dbParam = "<?php echo $var?>";

            gamePieces++;

            if(gamePieces>5){
              var sceneEl = document.querySelector('#treeText');


              xmlhttp = new XMLHttpRequest();
              xmlhttp.onreadystatechange = function() {

                if (this.readyState == 4 && this.status == 200) {

                  var myObj = JSON.parse(this.responseText);
                  sceneEl.setAttribute("text", {value: "BINARI"+"-"+myObj['c1']+myObj['c2']+myObj['c3']+myObj['c4']+myObj['c5']});
                }
              };
              xmlhttp.open("GET", "getWord.php?rid="+dbParam+"&sid=1", true);
              xmlhttp.send();
            }else{

            }
                        lastIndex = (lastIndex + 1) % COLORS.length;
            //this.setAttribute('material', 'color', COLORS[lastIndex]);
            gamePieces++;
            this.setAttribute("visible", false);
            console.log('I was clicked at: ', evt.detail.intersection.point);

          });
        }
      });

    </script>
    <link href="https://fonts.googleapis.com/css?family=Voces" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto%7CRoboto+Mono" media="all" />
  </head>
  <body class="a-body ">
    <a-scene>
      <a-assets>
        <a-asset-item id="myTree" src="assets/tree.ply"></a-asset-item>
        <a-asset-item id="myRobot" src="assets/codingrobots.ply"></a-asset-item>
      </a-assets>
      <!-- Textured plane parallel to ground. -->
      <a-entity scale="0.1 0.1 0.1" ply-model="src: #myRobot" position="-2 0 -2" rotation="-90 0 0"  sound="src: url(assets/bso_sonicpi.mp3); autoplay: true; loop: true"></a-entity>
      <!--<a-entity scale="0.1 0.1 0.1" cursor-listener ply-model="src: #myTree" position="2 0 -6" rotation="-90 0 0"></a-entity>-->
      <!--camera entity-->
      <a-entity>
        <a-entity camera look-controls wasd-controls id="user-cam"  camera position="0 1 5">
          <a-entity position="0 0 -3" scale="0.2 0.2 0.2" geometry="primitive: ring; radiusOuter: 0.20; radiusInner: 0.13;" material="color: #ADD8E6; shader: flat" cursor="maxDistance: 30; fuse: true">
            <a-animation begin="click" easing="ease-in" attribute="scale" fill="backwards" from="0.1 0.1 0.1" to="1 1 1" dur="150"></a-animation>
            <a-animation begin="fusing" easing="ease-in" attribute="scale" fill="forwards" from="1 1 1" to="0.2 0.2 0.2" dur="1500"></a-animation>
          </a-entity>
           <a-text text="value: Robot '<?php echo $var?>'" id="robotID" value="" position="-0.4 -0.4 -4" color="lightyellow" my-tree-component ></a-text>
          <a-text id="treeText" value="" position="-0.6 0.3 -4" color="lightyellow" my-tree-component ></a-text>
        </a-entity>
      </a-entity>
     <!-- <a-entity camera position="0 1 5">
        <a-entity cursor="fuse: true; fuseTimeout: 500"
                  position="0 0 -1"
                  geometry="primitive: ring; radiusInner: 0.026; radiusOuter: 0.03"
                  material="color: lightyellow; shader: flat">
        </a-entity>
        <a-text id="treeText" value="TREE" position="-0.3 0.3 -4" color="lightyellow" my-tree-component ></a-text>
      </a-entity>-->
      <a-entity>
        <a-entity cursor-listener position="5 0 0"  sound="src: url(assets/laser.mp3); on: click">
          <a-entity text="value: Microchip;"></a-entity>
          <a-box color="tomato" depth="0.5" height="1.5" width="0.5"></a-box>
        </a-entity>
        <!--<a-animation attribute="rotation"
                     dur="10000"
                     fill="forwards"
                     to="0 360 0"
                     repeat="indefinite"></a-animation>-->
       <a-animation attribute="position" from="0 7 0" to="0 8 0" dur="30000" repeat="indefinite"></a-animation>
      </a-entity>
      <a-entity>
          <a-entity text="value: Sensors;"></a-entity>
        <a-entity cursor-listener position="5 0 0"  sound="src: url(assets/laser.mp3); on: click">
          <a-box color="green" depth="0.5" height="1.5" width="0.5"></a-box>

        </a-entity>
      <a-animation attribute="position" from="-10 7 0" to="-10 5 0" dur="20000" repeat="indefinite"></a-animation>
      </a-entity>
      <a-entity>
          <a-entity text="value: Actuadors;"></a-entity>
        <a-entity cursor-listener position="5 0 0"  sound="src: url(assets/laser.mp3); on: click">
          <a-box color="yellow" depth="0.5" height="1.5" width="0.5"></a-box>
        </a-entity>
     <!--<a-animation attribute="rotation" dur="30000" fill="forwards" to="0 360 0" repeat="indefinite"></a-animation>-->

      </a-entity>
      <a-entity>
          <a-entity text="value: Batería;"></a-entity>
        <a-entity cursor-listener position="5 0 0" sound="src: url(assets/laser.mp3); on: click">
          <a-box color="orange" depth="0.5" height="1.5" width="0.5"></a-box>
          <a-entity text="value: Batería;"></a-entity>
        </a-entity>
      <a-animation attribute="position" from="0 7 -5" to="0 8 -5" dur="18000" repeat="indefinite"></a-animation>
      </a-entity>

      <a-sky color="#17CC87"></a-sky>
    <a-entity environment="lightPosition: 0 5 -2; groundColor: #445"></a-entity>

      <a-entity environment="preset: tron;"></a-entity>
      <a-entity id="stars" visible="false"></a-entity>

    </a-scene>
  </body>
</html>
