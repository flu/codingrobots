#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "ArduinoJson.h"
#include <Servo.h>
 

 const char* ssid = "";
 const char* password = "";


const char* host = "coding.futurelearningunit.com";

#define EYE1 D5
#define EYE2 D3

Servo servo;
int robotChecked = 0;
String robotID = "58";

int getCode = 0;

void setup () {


  Serial.begin(115200);
  WiFi.begin(ssid, password);
  
  servo.attach(2); //D4
  //servo.write(90);
  
  while (WiFi.status() != WL_CONNECTED) {
 
    delay(1000);
    Serial.println("Connecting..");
 
  }

  pinMode(EYE1, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  pinMode(EYE2, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  getCode = 0;
 
}
 
void loop() {
 
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

 
    HTTPClient http;  //Declare an object of class HTTPClient

 if(robotChecked==1){
   
    http.begin("http://codingrobots.futurelearningunit.com/getCode.php?rid="+robotID);  //Specify request destination
    int httpCode = http.GET();                                                                  //Send the request
 
    if (httpCode > 0) { //Check the returning code
      
      StaticJsonBuffer<300> JSONBuffer; 
      String JSONMessage = http.getString();   //Get the request response payload
      Serial.println(JSONMessage);                     //Print the response payload
      JsonObject& parsed = JSONBuffer.parseObject(JSONMessage); //Parse message
      
        
      if(parsed["ready"]=="1"){
        String secretCode = parsed["letter"]; //Get sensor type value
        char commandChar = secretCode.charAt(0);

        //const char* world = object["hello"];
        getCode = 1;
        Serial.println("Go to Sequence");
        //sequenceWord(0);
        switch(commandChar){
          case 'C': sequenceWord(1);Serial.println("C");
          break;
         case 'O':  sequenceWord(2);Serial.println("0");
          break;
          case 'D': sequenceWord(3);Serial.println("D");
          break;
          case 'I':  sequenceWord(4);Serial.println("I");
          break;
          case 'N':  sequenceWord(5);Serial.println("N");
          break;
          case 'G':  sequenceWord(6);Serial.println("G");
          break;
          case 'R':  sequenceWord(7);Serial.println("R");
          break;
          case 'B':  sequenceWord(8);Serial.println("B");
          break;
          case 'T':  sequenceWord(9);Serial.println("T");
          break;
          case 'S': sequenceWord(10);Serial.println("S");
          break;
        }
        
      }else{
        //Serial.println("No Code");
      }
 
 
    }
 }else{
  // Robot Activate
    http.begin("http://codingrobots.futurelearningunit.com/activateRobot.php?rid="+robotID);  //Specify request destination
    int httpCode = http.GET();                                                                  //Send the request
     robotChecked = 1;
    if (httpCode > 0) { //Check the returning code
      robotChecked = 1;
      sequenceWord(0);
    }
  
 }
 
    http.end();   //Close connection

    // Do some reaction in the motor and leds
 
  }
 
  delay(3000);    //Send a request every 30 seconds
 
}

void sequenceWord(int codeSequence){
Serial.print("Code Sequence:");
Serial.println(codeSequence);
switch(codeSequence){
  case 0:
    for(int i=1;i<3;i++){
       servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(500);                      // Wait for a second
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(500);    
      i++;
     }
  break;
  case 1:/* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);    
        /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);   
       
       
  break;
  case 2:
    /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000); 
      /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000); 
      /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000); 
      /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000); 
  break;
  case 3:/* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000); 
       /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
        /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
  break;
  case 4:/* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
        /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000); 
      /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
        /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      
  break;
  case 5:/* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
        /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
  break;
  case 6:/* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
  break;
  case 7:
  /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
  break;
  case 8:   /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
          /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
          /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
        /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
          /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
  break;
  case 9: /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
        /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
        /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
         /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
  break;
  case 10:
   /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
        /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
         /* 0 */ 
      servo.write(0);
      delay(1000);
      servo.write(180);
      delay(1000);
       servo.write(90);
       delay(1000);
       /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
      /* 1 */
       digitalWrite(EYE1, HIGH);   // Turn the LED on (Note that LOW is the voltage level
       digitalWrite(EYE2, HIGH);   // Turn the LED on (Note that LOW is the voltage level
      delay(2000);
      digitalWrite(EYE1, LOW);  // Turn the LED off by making the voltage HIGH
      digitalWrite(EYE2, LOW);  // Turn the LED off by making the voltage HIGH
      delay(1000);
  break;
}
  delay(3000);
}

